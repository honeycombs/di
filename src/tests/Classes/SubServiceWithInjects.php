<?php

declare(strict_types=1);

namespace Tests\Honeycombs\DI\Classes;

use Tests\Honeycombs\DI\Classes\Injectable\InjectableClass;

/**
 * Class ServiceWithInjects
 * Test class with injects to test third level injecting
 */
class SubServiceWithInjects
{
    /**
     * Simple injected class
     *
     * @var InjectableClass
     *
     * @inject
     */
    public $test;
}
