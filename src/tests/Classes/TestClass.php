<?php

declare(strict_types=1);

namespace Tests\Honeycombs\DI\Classes;

use Tests\Honeycombs\DI\Classes\Injectable\InjectableAsClass as Alias;
use Tests\Honeycombs\DI\Classes\Injectable\InjectableClass;
use Tests\Honeycombs\DI\Classes\Injectable\InjectableClass as SameAlias;
use Tests\Honeycombs\DI\Classes\NeighbourInjectableAsClass as NeighbourAlias;

class TestClass
{
    /**
     * Injected service with injects inside
     *
     * @var ServiceWithInjects
     *
     * @inject
     */
    public $serviceWithInjects;
    /**
     * Injected class with full namespace with public access
     *
     * @var \Tests\Honeycombs\DI\Classes\Injectable\InjectableClass
     *
     * @inject
     */
    public $publicInjectableServiceWithFullNamespaceDoc;

    /**
     * Injected class with alias with public access
     *
     * @var Alias
     *
     * @inject
     */
    public $publicInjectableServiceWithAliasNamespaceDoc;

    /**
     * Injected class with full namespace with private access
     *
     * @var \Tests\Honeycombs\DI\Classes\Injectable\InjectableClass
     *
     * @inject
     */
    private $privateInjectableServiceWithFullNamespaceDoc;

    /**
     * Injected class with alias with public access, with same injected service with another alias
     *
     * @var SameAlias
     *
     * @inject
     */
    public $publicInjectableServiceCopyWithAliasNamespaceDoc;

    /**
     * Non-injected property
     *
     * @var \Tests\Honeycombs\DI\Classes\NonInjectableClass
     */
    public $nonInjectableService;

    /**
     * Injected class with short namespace with public access
     *
     * @var InjectableClass
     *
     * @inject
     */
    public $publicInjectableServiceWithShortNamespaceDoc;

    /**
     * Injected class in same namespace with private access
     *
     * @var NeighbourInjectableClass
     *
     * @inject
     */
    private $privateNeighbourInjectableService;

    /**
     * Injected class in same namespace with public access
     *
     * @var NeighbourInjectableClass
     *
     * @inject
     */
    public $publicNeighbourInjectableService;

    /**
     * Injected class with alias in same namespace with public access
     *
     * @var NeighbourAlias
     *
     * @inject
     */
    public $publicNeighbourInjectableAliasedService;

    /**
     * Injected class with short namespace with private access
     *
     * @var InjectableClass
     *
     * @inject
     */
    private $privateInjectableServiceWithShortNamespaceDoc;

    public function getPrivateInjectableServiceWithFullNamespace(): InjectableClass
    {
        return $this->privateInjectableServiceWithFullNamespaceDoc;
    }

    public function getPrivateInjectableServiceWithShortNamespace(): InjectableClass
    {
        return $this->privateInjectableServiceWithShortNamespaceDoc;
    }

    public function getPrivateNeighbourInjectableService(): NeighbourInjectableClass
    {
        return $this->privateNeighbourInjectableService;
    }
}
