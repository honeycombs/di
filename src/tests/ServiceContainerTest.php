<?php

declare(strict_types=1);

namespace Tests\Honeycombs\DI;

use Honeycombs\DI\ServiceContainer;
use PHPUnit\Framework\TestCase;
use Tests\Honeycombs\DI\Classes\Injectable\InjectableAsClass;
use Tests\Honeycombs\DI\Classes\Injectable\InjectableClass;
use Tests\Honeycombs\DI\Classes\NeighbourInjectableAsClass;
use Tests\Honeycombs\DI\Classes\NeighbourInjectableClass;
use Tests\Honeycombs\DI\Classes\ServiceWithInjects;
use Tests\Honeycombs\DI\Classes\SubServiceWithInjects;
use Tests\Honeycombs\DI\Classes\TestClass;

/**
 * @covers \Honeycombs\DI\ServiceContainer
 * @coversDefaultClass \Honeycombs\DI\ServiceContainer
 */
class ServiceContainerTest extends TestCase
{
    /**
     * Tests that injects resolved properly
     *
     * @return void
     *
     * @covers ::resolveInjects
     */
    public function testInjecting(): void
    {
        $serviceContainer = new ServiceContainer();
        $testClass = new TestClass();
        $serviceContainer->resolveInjects($testClass);

        $injectedService = $testClass->publicInjectableServiceWithFullNamespaceDoc;
        $injectedNeighbourService = $testClass->publicNeighbourInjectableService;
        $injectedAsService = $testClass->publicInjectableServiceWithAliasNamespaceDoc;
        $injectedNeighbourAsService = $testClass->publicNeighbourInjectableAliasedService;

        $this->assertInstanceOf(InjectableClass::class, $testClass->publicInjectableServiceWithFullNamespaceDoc);
        $this->assertSame($injectedService, $testClass->publicInjectableServiceWithFullNamespaceDoc);

        $this->assertInstanceOf(InjectableClass::class, $testClass->getPrivateInjectableServiceWithFullNamespace());
        $this->assertSame($injectedService, $testClass->getPrivateInjectableServiceWithFullNamespace());

        $this->assertNull($testClass->nonInjectableService);

        $this->assertInstanceOf(InjectableClass::class, $testClass->publicInjectableServiceWithShortNamespaceDoc);
        $this->assertSame($injectedService, $testClass->publicInjectableServiceWithShortNamespaceDoc);

        $this->assertInstanceOf(InjectableClass::class, $testClass->getPrivateInjectableServiceWithShortNamespace());
        $this->assertSame($injectedService, $testClass->getPrivateInjectableServiceWithShortNamespace());

        $this->assertInstanceOf(NeighbourInjectableClass::class, $testClass->publicNeighbourInjectableService);
        $this->assertSame($injectedNeighbourService, $testClass->publicNeighbourInjectableService);

        $this->assertInstanceOf(NeighbourInjectableClass::class, $testClass->getPrivateNeighbourInjectableService());
        $this->assertSame($injectedNeighbourService, $testClass->getPrivateNeighbourInjectableService());

        $this->assertInstanceOf(InjectableAsClass::class, $testClass->publicInjectableServiceWithAliasNamespaceDoc);
        $this->assertSame($injectedAsService, $testClass->publicInjectableServiceWithAliasNamespaceDoc);

        $this->assertInstanceOf(NeighbourInjectableAsClass::class, $testClass->publicNeighbourInjectableAliasedService);
        $this->assertSame($injectedNeighbourAsService, $testClass->publicNeighbourInjectableAliasedService);

        $this->assertInstanceOf(InjectableClass::class, $testClass->publicInjectableServiceCopyWithAliasNamespaceDoc);
        $this->assertSame($injectedService, $testClass->publicInjectableServiceCopyWithAliasNamespaceDoc);

        $serviceWithInjects = $testClass->serviceWithInjects;
        $this->assertInstanceOf(ServiceWithInjects::class, $serviceWithInjects);
    }

    /**
     * Tests set and get service
     *
     * @return void
     *
     * @covers ::setService
     * @covers ::getService
     */
    public function testSet(): void
    {
        $serviceContainer = new ServiceContainer();
        $testClass = new TestClass();
        $testClass->publicNeighbourInjectableAliasedService = 100;
        $serviceContainer->setService($testClass);
        $this->assertSame($testClass, $serviceContainer->getService(TestClass::class));
    }

    /**
     * Test recursive injecting
     *
     * @return void
     *
     * @covers ::resolveInjects
     */
    public function testSubInjecting(): void
    {
        $serviceContainer = new ServiceContainer();
        $parentClass = new TestClass();
        $serviceContainer->resolveInjects($parentClass);

        $testClass = $parentClass->serviceWithInjects;
        $this->assertInstanceOf(ServiceWithInjects::class, $testClass);

        $injectedService = $testClass->publicInjectableServiceWithFullNamespaceDoc;
        $injectedNeighbourService = $testClass->publicNeighbourInjectableService;
        $injectedAsService = $testClass->publicInjectableServiceWithAliasNamespaceDoc;
        $injectedNeighbourAsService = $testClass->publicNeighbourInjectableAliasedService;

        $this->assertInstanceOf(InjectableClass::class, $testClass->publicInjectableServiceWithFullNamespaceDoc);
        $this->assertSame($injectedService, $testClass->publicInjectableServiceWithFullNamespaceDoc);

        $this->assertInstanceOf(InjectableClass::class, $testClass->getPrivateInjectableServiceWithFullNamespace());
        $this->assertSame($injectedService, $testClass->getPrivateInjectableServiceWithFullNamespace());

        $this->assertNull($testClass->nonInjectableService);

        $this->assertInstanceOf(InjectableClass::class, $testClass->publicInjectableServiceWithShortNamespaceDoc);
        $this->assertSame($injectedService, $testClass->publicInjectableServiceWithShortNamespaceDoc);

        $this->assertInstanceOf(InjectableClass::class, $testClass->getPrivateInjectableServiceWithShortNamespace());
        $this->assertSame($injectedService, $testClass->getPrivateInjectableServiceWithShortNamespace());

        $this->assertInstanceOf(NeighbourInjectableClass::class, $testClass->publicNeighbourInjectableService);
        $this->assertSame($injectedNeighbourService, $testClass->publicNeighbourInjectableService);

        $this->assertInstanceOf(NeighbourInjectableClass::class, $testClass->getPrivateNeighbourInjectableService());
        $this->assertSame($injectedNeighbourService, $testClass->getPrivateNeighbourInjectableService());

        $this->assertInstanceOf(InjectableAsClass::class, $testClass->publicInjectableServiceWithAliasNamespaceDoc);
        $this->assertSame($injectedAsService, $testClass->publicInjectableServiceWithAliasNamespaceDoc);

        $this->assertInstanceOf(NeighbourInjectableAsClass::class, $testClass->publicNeighbourInjectableAliasedService);
        $this->assertSame($injectedNeighbourAsService, $testClass->publicNeighbourInjectableAliasedService);

        $this->assertInstanceOf(InjectableClass::class, $testClass->publicInjectableServiceCopyWithAliasNamespaceDoc);
        $this->assertSame($injectedService, $testClass->publicInjectableServiceCopyWithAliasNamespaceDoc);

        $this->assertInstanceOf(SubServiceWithInjects::class, $testClass->subServiceWithInjects);
        $this->assertInstanceOf(InjectableClass::class, $testClass->subServiceWithInjects->test);
        $this->assertEquals($injectedService, $testClass->subServiceWithInjects->test);
    }
}
