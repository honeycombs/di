<?php

declare(strict_types=1);

namespace Honeycombs\DI;

use ReflectionProperty;

/**
 * Class ClassNamesResolver
 * Parsing PHPDocs to collect injectable properties and class uses
 * @todo support namespace grouping
 * @todo tests
 */
class ClassNamesResolver
{
    /**
     * Parsed classNames in USE part grouped by classNames
     *
     * @var string[][]
     */
    private $parsedUses;

    /**
     * Parses property PHPDoc to get injecting service className if property must be replaced with service instance
     *
     * @param ReflectionProperty $property Property to examine
     * @return InjectableProperty
     */
    public function parseProperty(ReflectionProperty $property): InjectableProperty
    {
        $injectableDocExists = false;
        $className = null;

        $comment = $property->getDocComment() ?? '';

        foreach (explode("\n", $comment) as $item) {
            if (strpos($item, '@inject')) {
                $injectableDocExists = true;
            }

            if (strpos($item, '@var')) {
                $className = trim(substr($item, strpos($item, '@var') + 4));

                if (strpos($className, '\\') === false) {
                    $className = $this->getFullClassName($property, $className);
                }
            }
        }

        $injectableProperty = new InjectableProperty(
            $property->getName(),
            $className,
            $injectableDocExists && $className && !$property->isStatic()
        );

        return $injectableProperty;
    }

    /**
     * Gets full class name of injecting service
     *
     * @param ReflectionProperty $property Property to replace with injecting service
     * @param string $className Short class name of injecting service
     * @return string Full class name of injecting service
     */
    private function getFullClassName(ReflectionProperty $property, string $className): ?string
    {
        $uses = $this->getUses($property);

        if (!isset($uses[$className])) {
            $neighbourClassName = $property->getDeclaringClass()->getNamespaceName() . '\\' . $className;

            if (class_exists($neighbourClassName)) {
                return $neighbourClassName;
            }
        } else {
            return $uses[$className];
        }

        return null;
    }

    /**
     * Parses file to get all classes in USE part
     *
     * @param ReflectionProperty $property Property in target class being processed
     * @return string[] mapping short class names or aliases to full class names
     */
    private function getUses(ReflectionProperty $property): array
    {
        $classToInject = $property->getDeclaringClass()->getNamespaceName() . $property->getDeclaringClass()->getName();

        if (!isset($this->parsedUses[$classToInject])) {
            $buildNamespace = false;
            $buildAlias = false;
            $namespace = '';
            $alias = '';
            $lastPart = '';
            $namespaces = [];

            foreach (token_get_all(file_get_contents($property->getDeclaringClass()->getFileName())) as $token) {
                if ($token === ';') {
                    $buildNamespace = false;
                    $buildAlias = false;

                    if ($namespace) {
                        $namespaces[$alias !== '' ? $alias : $lastPart] = '\\' . $namespace;
                        $namespace = '';
                        $alias = '';

                        continue;
                    }
                    $namespace = '';
                    $alias = '';
                }

                if ($token[0] === T_USE) {
                    $buildNamespace = true;

                    continue;
                }

                if ($token[0] === T_AS) {
                    $buildAlias = true;

                    continue;
                }

                if ($buildNamespace) {
                    $lastPart = trim($token[1]);

                    if ($buildAlias) {
                        $alias .= $lastPart;
                    } else {
                        $namespace .= $lastPart;
                    }

                    continue;
                }
            }
            $this->parsedUses[$classToInject] = $namespaces;
        }

        return $this->parsedUses[$classToInject];
    }
}
