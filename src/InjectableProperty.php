<?php

declare(strict_types=1);

namespace Honeycombs\DI;

/**
 * Class InjectableProperty
 * Data-class to store ClassNameResolver property parsing result
 * @todo tests
 */
class InjectableProperty
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string|null
     */
    private $className;

    /**
     * @var bool
     */
    private $injectable = false;

    /**
     * InjectableProperty constructor.
     * @param string $name Property name
     * @param string $className Class name of injecting service
     * @param bool $injectable Flag that property can be injected
     */
    public function __construct(string $name, ?string $className, bool $injectable)
    {
        $this->name = $name;
        $this->className = $className;
        $this->injectable = $injectable;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getClassName(): ?string
    {
        return $this->className;
    }

    /**
     * @return bool
     */
    public function isInjectable(): bool
    {
        return $this->injectable;
    }
}
