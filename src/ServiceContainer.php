<?php

declare(strict_types=1);

namespace Honeycombs\DI;

use ReflectionClass;
use ReflectionProperty;

/**
 * @todo avoid recursive injecting
 * @todo support inject right of @var declaration together with @inject
 * @todo readme
 * @todo test static & protected properties
 * @todo tests rewrite for nice readable namings
 * @todo errors throwing
 */
class ServiceContainer
{
    /**
     * Instances of services
     *
     * @var array
     */
    private static $serviceInstances = [];

    /**
     * Class name resolvers
     *
     * @var ClassNamesResolver
     */
    private $classNameResolver;

    /**
     * ServiceContainer constructor.
     */
    public function __construct()
    {
        $this->classNameResolver = new ClassNamesResolver();
    }

    /**
     * Sets service to future inject
     *
     * @param mixed $service Service to set
     * @return $this
     */
    public function setService($service)
    {
        $serviceClass = $this->normalizeServiceName(get_class($service));
        self::$serviceInstances[$serviceClass] = $service;

        return $this;
    }

    /**
     * Gets service by class
     *
     * @param string $serviceClassName Service to set
     * @return mixed
     */
    public function getService(string $serviceClassName)
    {
        return self::$serviceInstances[$this->normalizeServiceName($serviceClassName)] ?? null;
    }

    /**
     * Adds "\" to class name first position if not exists
     *
     * @param string $serviceClass
     * @return string
     */
    private function normalizeServiceName(string $serviceClass): string
    {
        if (substr($serviceClass, 0, 1) !== '\\') {
            $serviceClass = '\\' . $serviceClass;
        }

        return $serviceClass;
    }

    /**
     * Parses instance class to inject all typehinted services.
     * Injects all services recursive in injected classes
     *
     * @param mixed $targetClass Class instance to process injects
     * @return void
     */
    public function resolveInjects($targetClass): void
    {
        $instance = new ReflectionClass($targetClass);

        foreach ($instance->getProperties() as $property) {
            $injectableProperty = $this->classNameResolver->parseProperty($property);

            if ($injectableProperty->isInjectable()) {
                $injectableService = $this->getServiceInstance($injectableProperty->getClassName());
                $this->injectService($targetClass, $property, $injectableService);
            }
        }
    }

    /**
     * Gets instance by class name
     *
     * @param string $injectableClassName Class name for service that must be injected
     * @return mixed Instance
     */
    private function getServiceInstance(string $injectableClassName)
    {
        if (!isset(static::$serviceInstances[$injectableClassName])) {
            $injectedService = new $injectableClassName();
            $this->resolveInjects($injectedService);
            static::$serviceInstances[$injectableClassName] = $injectedService;
        }

        return static::$serviceInstances[$injectableClassName];
    }

    /**
     * Inject instance instead of property with @inject PHPDoc
     *
     * @param mixed $targetClass Target class services injected to
     * @param ReflectionProperty $property Property with @inject PHPDoc
     * @param mixed $injectableService Service instance to inject instead of property
     * @return void
     */
    private function injectService($targetClass, ReflectionProperty $property, $injectableService): void
    {
        if (!$property->isPublic()) {
            $property->setAccessible(true);
            $property->setValue($targetClass, $injectableService);
            $property->setAccessible(false);
        } else {
            $property->setValue($targetClass, $injectableService);
        }
    }
}
